import React from 'react';
import Typography from '@material-ui/core/Typography';

const Error = props => {
  return (
    <Typography component="p" style={{color:'red'}}>
      {props.message}
    </Typography>

  )
}

export default Error;
