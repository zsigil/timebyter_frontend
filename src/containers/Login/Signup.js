import React, {Component} from 'react';
import { Link as RouterLink } from 'react-router-dom'
import { connect } from 'react-redux';

import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import Link from '@material-ui/core/Link';
// import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';

import * as actions from '../../store/actions/auth';
import Error from './Error';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',}
    },
  paper: {
    marginTop: theme.spacing.unit * 6,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  progress: {
    marginTop: '25vh',
    marginLeft: '50%'
},

});

class Signup extends Component {
  state ={
    username: '',
    email: '',
    password1: '',
    password2: ''
  }

  componentDidMount() {
      ValidatorForm.addValidationRule('isPasswordLongEnoughAndHasLetterAndNumber', (value) => {
        const re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
        return re.test(value);
      });

      ValidatorForm.addValidationRule('isUsernameLongEnough', (value) => {
          if (value.length < 6) {
              return false;
          }
          return true;
      });

      ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
          if (value !== this.state.password1) {
              return false;
          }
          return true;
        });

      ValidatorForm.addValidationRule('isEmail', (value) => {
        const re = /\S+@\S+\.\S+/;
        return re.test(value);
        });
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.onSignup(this.state.username,this.state.email, this.state.password1, this.state.password2)
  }

  handleInput = e =>{
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  render(){
    const { classes, error } = this.props;
    let fullerror = "";
    let errormessage = "Something went wrong...";

    if (error) {
      fullerror = Object.values(error);

      errormessage = fullerror[2].data.username ?  "Username or email already exists." : "";
      errormessage = fullerror[2].data.email ? "Username or email already exists.": "";
      errormessage += fullerror[2].data.password1 ? fullerror[2].data.password1[0] : "";

    }
    return(
      <div>
      {this.props.loading ?
         <CircularProgress className={classes.progress} /> :

      <main className={classes.main}>
        <Paper className={classes.paper}>
          {error ? <Error message={errormessage}/> : null}
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
          <ValidatorForm onSubmit={this.handleSubmit} className={classes.form} autoComplete="off">
            <TextValidator
                margin="normal"
                required
                fullWidth
                label="Username"
                id="username"
                name="username"
                onChange={this.handleInput}
                value={this.state.username}
                autoFocus
                validators={['isUsernameLongEnough']}
                errorMessages={['username must be min 6 chars']}/>
            <TextValidator
                margin="normal"
                required
                fullWidth
                label="Email"
                id="email"
                name="email"
                type="email"
                onChange={this.handleInput}
                value={this.state.email}
                validators={['isEmail']}
                errorMessages={['email address is needed']}
                autoFocus />
            <TextValidator
                margin="normal"
                required
                fullWidth
                label="Password"
                name="password1"
                type="password"
                id="password1"
                onChange={this.handleInput}
                value={this.state.password1}
                validators={['isPasswordLongEnoughAndHasLetterAndNumber']}
                errorMessages={['password must be min 8 chars, contain at least 1 number and 1 letter']}/>
            <TextValidator
                margin="normal"
                required
                fullWidth
                label="Confirm password"
                name="password2"
                type="password"
                id="password2"
                onChange={this.handleInput}
                value={this.state.password2}
                validators={['isPasswordMatch', 'required']}
                errorMessages={['password mismatch', 'this field is required']}/>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Link component={RouterLink} to="/login" underline='none'>
            <Button
              fullWidth
              color="primary"
              className={classes.submit}
            >
              Go to Login
            </Button>
            </Link>
          </ValidatorForm>
        </Paper>
      </main>
      }

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSignup:  (username,email,password1, password2) => dispatch(actions.authSignup(username,email,password1, password2)),
  };
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Signup));
