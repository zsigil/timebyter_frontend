import React, {Component} from 'react';
import { Link as RouterLink } from 'react-router-dom'
import { connect } from 'react-redux';

//import PropTypes from 'prop-types';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';

import * as actions from '../../store/actions/auth';
import Error from './Error';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',}
    },
  paper: {
    marginTop: theme.spacing.unit * 6,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  progress: {
  marginTop: '25vh',
  marginLeft: '50%'
  },

});

class Login extends Component {
  state ={
    username: '',
    password: ''
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onAuth(this.state.username,this.state.password)
  }

  handleInput = e =>{
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  render(){
    const { classes, error } = this.props;

    return(
      <div>
      {this.props.loading ?
         <CircularProgress className={classes.progress} /> :

      <main className={classes.main}>
        <Paper className={classes.paper}>
          {error ? <Error message="Username or password is incorrect"/> : null}
          <Typography component="h1" variant="h5">
            Log in
          </Typography>
          <form onSubmit={this.handleSubmit} className={classes.form} autoComplete="off" >
            <TextField
                margin="normal"
                required
                fullWidth
                label="Username"
                id="username"
                name="username"
                onChange={this.handleInput}
                value={this.state.username}

                autoFocus />
            <TextField
                margin="normal"
                required
                fullWidth
                label="Password"
                name="password"
                type="password"
                id="password"
                onChange={this.handleInput}
                value={this.state.password}/>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Log in
            </Button>
            <Link component={RouterLink} to="/signup" underline='none'>
              <Button
                fullWidth
                color="primary"
                className={classes.submit}
              >
                Go to Sign up
              </Button>
            </Link>
          </form>
        </Paper>
      </main>
      }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth:  (username, password) => dispatch(actions.authLogin(username,password)),
  };
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Login));
