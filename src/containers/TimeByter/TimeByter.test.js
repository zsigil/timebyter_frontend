import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import {TimeByter} from './TimeByter';
import Projects from '../../components/Projects/Projects';

configure({ adapter: new Adapter() });


describe('<TimeByter />', ()=>{
  const props = {
    classes: {
      root: {},
    },
    onInitProjectList: ()=>{},
  }

  it('renders without crashing', () => {

     shallow(<TimeByter {...props}/>);
   });

  it('should render <Projects/>', ()=>{

    const wrapper = shallow(<TimeByter {...props}/>);
    expect(wrapper.find(Projects)).toHaveLength(1);
  });

});
