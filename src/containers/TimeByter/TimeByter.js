import React , { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';

import * as actions from '../../store/actions/index';
import Projects from '../../components/Projects/Projects';

const styles = {
  root: {
    width: '80%',
    maxWidth: 600,
    // textAlign: 'center',
    margin: 'auto',
  },
};

export class TimeByter extends Component {

  componentDidMount(){
    this.props.onInitProjectList(this.props.token)
  }

  render(){
    return(
      <div className={this.props.classes.root}>
        <Projects
          projects={this.props.projects}
        />
      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    projects: state.timebyter.projects,
    token: state.auth.token
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onInitProjectList:  (token) => dispatch(actions.initProjectList(token)),
  };
}


export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(TimeByter));
