import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as actions from './store/actions/auth';


import Layout from './components/Layout/Layout';
import Welcome from './components/Welcome/Welcome';
import TimeByter from './containers/TimeByter/TimeByter';
import NewProject from './components/Projects/NewProject/NewProject';
import Project from './components/Projects/Project/Project';
import Login from './containers/Login/Login';
import Signup from './containers/Login/Signup';
import UserProfile from './components/UserProfile/UserProfile';

export class App extends Component {

  componentDidMount(){
      this.props.onTryAutoSignUp()
  }

  render() {
    let routes = (
        <Switch>
          <Route exact path='/login' component={Login}/>
          <Route exact path='/signup' component={Signup}/>
          <Route exact path='/' component={Welcome}/>
          <Redirect to='/' />
        </Switch>
    );

    if (this.props.authenticated) {
      routes = (
        <Switch>
            <Route path='/profile' component={UserProfile}/>
            <Route path='/projects/new' component={NewProject}/>
            <Route path='/projects/:id' component={Project}/>
            <Route path='/projects'  component={TimeByter}/>
            <Redirect to='/projects' />

        </Switch>
      )
    }



    return (
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.token !== null
  }
}


const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignUp:  () => dispatch(actions.authCheckState()),
  };
}

export default withRouter((connect(mapStateToProps,mapDispatchToProps)(App)));
