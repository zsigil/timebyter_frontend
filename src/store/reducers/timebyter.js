const initialState = {
  projects : [],
  selectedProject : {
    projectId: null,
    tasks: []
  },
  timetrackingOn: false,
}

const reducer = (state=initialState, action) => {
  if(action.type==='SET_PROJECTS'){
    const newprojects = action.projects
    return {
      ...state,
      projects : newprojects
    }
  }
  if(action.type==='START_TRACKING'){
    return {
      ...state,
      timetrackingOn: true
    }
  }

  if(action.type==='STOP_TRACKING'){
    return {
      ...state,
      timetrackingOn: false
    }
  }

  return state;
}

export default reducer;
