import * as actionTypes from '../actions/actionTypes';

const initialState = {
  token: null,
  error: null,
  loading: false,
  username: null
}

const authStart = (state, action) =>{
  return {
    ...state,
    error: null,
    loading: true
  }
}

const authSuccess = (state,action)=>{
  return{
    ...state,
    error:null,
    token: action.token,
    loading: false,
    username: action.username
  }
}

const authFail = (state,action) => {
  return {
    ...state,
    error: action.error,
    loading: false
  }
}

const authLogout = (state, action)=> {
  return {
    ...state,
    username: null,
    token: null
  }
}


const reducer = (state=initialState, action)=>{
  switch (action.type) {
    case actionTypes.AUTH_START: return authStart(state,action);
    case actionTypes.AUTH_SUCCESS: return authSuccess(state,action);
    case actionTypes.AUTH_FAIL: return authFail(state,action);
    case actionTypes.AUTH_LOGOUT: return authLogout(state,action);

    default: return state;

  }
}

export default reducer;
