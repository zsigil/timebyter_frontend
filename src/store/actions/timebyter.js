import axios from 'axios';


export const initProjectList = (token) => {
  return dispatch => {

    axios.get("http://127.0.0.1:8000/api/projects/",
     {headers: {'Authorization': `Token ${token}`}
    })
      .then(response=>{
        console.log(response)
        dispatch(setProjects(response.data))
      })
      .catch(error=>{
        console.log(error)
      })
  }
}

export const setProjects= (projects) =>{
    return {
      type: 'SET_PROJECTS',
      projects: projects
    }
}

export const startTimeTracking = () => {
  return {
    type: 'START_TRACKING'
  }
}

export const stopTimeTracking = () => {
  return {
    type: 'STOP_TRACKING'
  }
}
