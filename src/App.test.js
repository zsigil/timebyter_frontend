import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import {App} from './App';

configure({ adapter: new Adapter() });


describe('<App />', ()=>{
  const props = {
    classes: {
      root: {},
      button: {}
    },
    onTryAutoSignUp: ()=>{},
  }

  it('renders without crashing', () => {

     shallow(<App {...props}/>);
   });

  it('should render 3 <Routes/> if not authenticated', ()=>{

    const wrapper = shallow(<App {...props}/>);
    expect(wrapper.find(Route)).toHaveLength(3);
  });

  it('should render 4 <Routes/> if authenticated', ()=>{

    const wrapper = shallow(<App authenticated={true} {...props}/>);
    expect(wrapper.find(Route)).toHaveLength(4);
  });

  it('should render 1 <Redirect /> if authenticated', ()=>{

    const wrapper = shallow(<App authenticated={true} {...props}/>);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

  it('should render 1 <Redirect /> if not authenticated', ()=>{

    const wrapper = shallow(<App authenticated={false} {...props}/>);
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });


    it('should render <Layout/>', ()=>{

      const wrapper = shallow(<App {...props}/>);
      expect(wrapper.find(Layout)).toHaveLength(1);
    });


});
