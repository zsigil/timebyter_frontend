import { configure, shallow,render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import Typography from '@material-ui/core/Typography';
import {FootBar} from './FootBar';

configure({ adapter: new Adapter() });


describe('FootBar', () => {

  it('renders without crashing', () => {
    const props = {
      classes: {
        root: {},
        appbar: {},
      }}

     shallow(<FootBar {...props}/>);
   });

  it('should render correctly', () => {
    const props = {
      classes: {
        root: {},
        appbar: {},
      }
    }

    const component = shallow(<FootBar {...props}/>);

    expect(component).toMatchSnapshot();
  });



  it('should contain Ildikó Zsigmond', ()=>{
    const props = {
      classes: {
        root: {},
        appbar: {},
      }
    }
    const wrapper = shallow(<FootBar {...props}/>);
    expect(wrapper.containsMatchingElement(<Typography>Ildikó Zsigmond 2019</Typography>)).toBeTruthy()
    })

});
