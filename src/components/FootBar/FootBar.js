import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
    // fontSize: '15px'
  },

  appbar: {
    top: 'auto',
    bottom: 0,
    background: 'transparent',
    boxShadow:'none',
  }
});

export class FootBar extends Component {


  render(){
    let year = new Date().getFullYear()

    return(
      <div className={this.props.classes.root}>
        <div>
          <AppBar className={this.props.classes.appbar} position="fixed">
            <Grid container justify="center">
              <Toolbar>
                <Grid item>
                  <Typography
                    variant="body2"
                    color='inherit'
                  >
                  Ildikó Zsigmond {year}
                  </Typography>
                </Grid>
              </Toolbar>
            </Grid>
          </AppBar>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(FootBar);
