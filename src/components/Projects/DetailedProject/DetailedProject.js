import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';


const styles = theme => ({
  card: {
  maxWidth: '80%',
  margin: 'auto',
  wordWrap: 'break-word',
  },
  section:{
    margin:  `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
  }
});

class DetailedProject extends Component {

  render() {
    console.log(this.props.project)
    return (
    <Card className={this.props.classes.card} raised={true}>
     <CardActionArea>
       <CardContent onClick={this.props.handleDetail}>
        <div className={this.props.classes.section}>
           <Typography gutterBottom variant="h5" component="h2">
             {this.props.project.title}
           </Typography>
           <Typography gutterBottom variant="h5">
             {this.props.project.project_duration}
           </Typography>
         </div>
       <div className={this.props.classes.section}>
         <Typography variant="body1">
           {this.props.project.description}
         </Typography>
       </div>
         <Divider variant="middle"/>

        <div className={this.props.classes.section}>
         <Typography variant="body1">
           Number of tasks: {this.props.project.number_of_tasks}
         </Typography>
         <Typography variant="body1">
          Started on: {this.props.project.start}
         </Typography>

         <Typography variant="body1" >
           Project is {this.props.project.finished? `finished on ${this.props.project.finish}`: 'not finished.'}
         </Typography>
       </div>
       </CardContent>
       </CardActionArea>
     </Card>

    );
  }
}

DetailedProject.propTypes = {
  classes: PropTypes.object.isRequired,
  handleDetail: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired
};

export default withStyles(styles)(DetailedProject);
