import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

// import classNames from 'classnames';
// import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



class NewProject extends Component {
  state = {
    project: {
      title: '',
      description: '',
    },
    open: true,
  }

  componentDidMount(){
    ValidatorForm.addValidationRule('projectTitleLength', (value) => {
        if (value.length > 50 || value.length<1 ) {
            return false;
        }
        return true;
    });
  }

  handleClose = () => {
    this.setState({open:false});
    this.props.history.replace('/projects');
  }

  handleInfo = () => {
    if (!this.state.project.title) {
      return;
    }

    const url = "http://127.0.0.1:8000/api/projects/"
    const headers = {
    "Content-Type": "application/json",
    "Authorization":  `Token ${this.props.token}`,
    }

    console.log(this.props.token)
    console.log(this.state.project)
    axios.post(url,
    this.state.project,
    {headers: headers}
    )
      .then(response=>{
        console.log(response)
        this.handleClose();
      })
      .catch(error=>{
        console.log(error)
      })

  }

  handleInputChange = (e) => {
      const newproject = {...this.state.project}
      newproject[e.target.name] = e.target.value
      this.setState({
        project: newproject
      })
  }


  render(){
    return(
      <div>

        <ValidatorForm
          onSubmit={this.handleInfo}>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          transitionDuration={700}
        >

          <DialogTitle id="form-dialog-title">Creating new project</DialogTitle>
          <DialogContent >
            <DialogContentText>
              Please provide a title and description for your project. Starting time will be set automatically.
            </DialogContentText>
            <TextValidator
              required
              autoFocus
              margin="dense"
              id="title"
              name="title"
              label="Project title"
              type="text"
              fullWidth
              value={this.state.project.title}
              onChange={this.handleInputChange}
              autoComplete="off"
              validators={['projectTitleLength']}
              errorMessages={['Required field, maximum 50 characters']}
            />
            <TextField
              margin="dense"
              id="description"
              name="description"
              label="Project description"
              type="text"
              fullWidth
              value={this.state.project.description}
              onChange={this.handleInputChange}
              autoComplete="off"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button type="submit" onClick={this.handleInfo} color="primary">
              Add new project
            </Button>
          </DialogActions>
        </Dialog>
        </ValidatorForm>


      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
  }
}


export default (connect(mapStateToProps,null)(NewProject));
