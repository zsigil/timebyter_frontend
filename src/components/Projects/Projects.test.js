import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import List from '@material-ui/core/List';
import { Link } from 'react-router-dom';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';

import {Projects} from './Projects';

configure({ adapter: new Adapter() });


describe('<Projects />', ()=>{
  const props = {
    classes: {
      fab: {},
      listitemtext:{},
    },
    projects: [],
  }

  it('renders without crashing', () => {

     shallow(<Projects {...props}/>);
   });

  it('should render a link', ()=>{

    const wrapper = shallow(<Projects {...props}/>);
    expect(wrapper.find(Link)).toHaveLength(1);
  });

  it('should render a Fab icon', ()=>{

    const wrapper = shallow(<Projects {...props}/>);
    expect(wrapper.find(Fab)).toHaveLength(1);
  });

  it('should have a link to /projects/new', ()=>{

    const wrapper = shallow(<Projects {...props}/>);
    expect(wrapper.contains(<Link to="/projects/new"><Fab color="primary" className={{}} size="small"><AddIcon /></Fab></Link>)).toEqual(true);
  });

});
