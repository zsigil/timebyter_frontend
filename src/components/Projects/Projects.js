import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import LaunchIcon from '@material-ui/icons/Launch';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';


const styles = theme => ({
    fab: {
      margin: theme.spacing.unit,
     },
    listitemtext:{
      maxWidth:'400px',
      wordWrap: 'break-word'
       },
});

export class Projects extends Component{

  render(){
    let mylist = this.props.projects.map(project=>(
      <ListItem key={project.id}>
        <ListItemText className={this.props.classes.listitemtext}
          primary={project.title}
          secondary={`Started on : ${project.start}`}
        />
        <ListItemSecondaryAction>
          <Link to={`/projects/${project.id}`}>
            <IconButton color="primary">
                <LaunchIcon />
            </IconButton>
          </Link>
        </ListItemSecondaryAction>
      </ListItem>
    ));

    return(
      <div>
        <Typography
          variant="h4"
        >
          Projects
          <Link to="/projects/new">
            <Fab
              color="primary"
              size="small"
              className={this.props.classes.fab}
            >
               <AddIcon />
            </Fab>
          </Link>
        </Typography>
        <List>
          {mylist}
        </List>
      </div>
    );
  }
}

Projects.propTypes = {
  projects: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

export default  withStyles(styles)(Projects);
