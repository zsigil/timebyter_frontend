import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';
// import classNames from 'classnames';
// import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



class EditProject extends Component {
    state = {
      project: {
        title: this.props.project.title? this.props.project.title : '',
        description: this.props.project.description? this.props.project.description : '',
      },
    }

    componentDidMount(){
      ValidatorForm.addValidationRule('projectTitleLength', (value) => {
          if (value.length > 50 || value.length<1 ) {
              return false;
          }
          return true;
      });
    }

    handleEdit = () => {
      if (!this.state.project.title) {
        return;
      }

      const url = `http://127.0.0.1:8000/api/projects/${this.props.project.id}/`
      const headers = {
      "Content-Type": "application/json",
      "Authorization":  `Token ${this.props.token}`,
      }

      axios.patch(url,
      this.state.project,
      {headers: headers}
      )
        .then(response=>{
          this.props.close();
        })
        .catch(error=>{
          console.log(error)
        })

    }

    handleInputChange = (e) => {
        const newproject = {...this.state.project}
        newproject[e.target.name] = e.target.value
        this.setState({
          project: newproject
        })
    }


    render(){
      return(
        <div>
          <ValidatorForm
            onSubmit={this.handleEdit}>
          <Dialog
            open={this.props.editing}
            onClose={this.props.close}
            transitionDuration={700}
          >

            <DialogTitle id="form-dialog-title">Editing project</DialogTitle>
            <DialogContent>
              <DialogContentText>
                You may edit the title and/or description of your project.
              </DialogContentText>
              <TextValidator
                autoFocus
                margin="dense"
                id="title"
                name="title"
                label="Project title"
                type="text"
                fullWidth
                value={this.state.project.title}
                onChange={this.handleInputChange}
                autoComplete="off"
                validators={['projectTitleLength']}
                errorMessages={['Required field, maximum 50 characters']}
              />
              <TextField
                margin="dense"
                id="description"
                name="description"
                label="Project description"
                type="text"
                fullWidth
                value={this.state.project.description}
                onChange={this.handleInputChange}
                autoComplete="off"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.close} color="primary">
                Cancel
              </Button>
              <Button type="submit" onClick={this.handleEdit} color="primary">
                Edit project
              </Button>
            </DialogActions>

          </Dialog>

        </ValidatorForm>
        </div>

      );
    }
  }

  const mapStateToProps = state => {
    return {
      token: state.auth.token,
    }
  }

  EditProject.propTypes = {
    close: PropTypes.func.isRequired,
    editing: PropTypes.bool.isRequired,
    project: PropTypes.object.isRequired
  };


  export default (connect(mapStateToProps,null)(EditProject));
