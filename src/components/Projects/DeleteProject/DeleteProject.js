import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class DeleteProject extends Component {
  render() {
    return (
      <div>
        <Dialog
          open={true}
          onClose={this.props.cancelDelete}
        >
          <DialogTitle>{`You are about to delete project "${this.props.projectName}"`}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              If you proceed, the project and all the tasks related to it will be
              permanently deleted and cannot be restored.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.cancelDelete} color="primary" autoFocus>
              Cancel
            </Button>
            <Button onClick={this.props.deleteProject} color="primary" >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DeleteProject.propTypes = {
  cancelDelete: PropTypes.func.isRequired,
  deleteProject: PropTypes.func.isRequired,
  projectName: PropTypes.string.isRequired
};

export default DeleteProject;
