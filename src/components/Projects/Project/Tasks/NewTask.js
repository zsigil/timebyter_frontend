import React, { Component } from 'react';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

// import classNames from 'classnames';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class NewTask extends Component {
  state = {
    task: {
      title: '',
      description: '',
    },
  }

  componentDidMount(){
    ValidatorForm.addValidationRule('taskTitleLength', (value) => {
      if (value.length > 50 || value.length<1 ) {
          return false;
      }
      return true;
    });
  }

  handleInputChange = (e) => {
      const newtask = {...this.state.task}
      newtask[e.target.name] = e.target.value
      this.setState({
        task: newtask
      })
  }

  render(){
    return(
      <div>
        <ValidatorForm
          onSubmit={()=>this.props.add(this.state.task)}>

        <Dialog
          open={this.props.dialogopen}
          onClose={this.props.handleClose}
          transitionDuration={700}
        >
          <DialogTitle id="form-dialog-title">Creating new task</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please provide a title and description for your task.
            </DialogContentText>
            <TextValidator
              autoFocus
              margin="dense"
              id="title"
              name="title"
              label="Task title"
              type="text"
              fullWidth
              value={this.state.task.title}
              onChange={this.handleInputChange}
              autoComplete="off"
              validators={['taskTitleLength']}
              errorMessages={['Required field, maximum 50 characters']}
            />
            <TextField
              margin="dense"
              id="description"
              name="description"
              label="Task description"
              type="text"
              fullWidth
              value={this.state.task.description}
              onChange={this.handleInputChange}
              autoComplete="off"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={()=>this.props.add(this.state.task)} color="primary">
              Add new task
            </Button>
          </DialogActions>
        </Dialog>
      </ValidatorForm>

      </div>

    );
  }
}

NewTask.propTypes = {
  handleClose: PropTypes.func.isRequired,
  dialogopen: PropTypes.bool.isRequired,
  add: PropTypes.func.isRequired,
};


export default NewTask;
