import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import List from '@material-ui/core/List';
import {Tasks} from './Tasks';
import Task from './Task/Task';

configure({ adapter: new Adapter() });


describe('<Tasks />', ()=>{
  const props = {
    classes: {
      root: {},
    },
    tasks:[{a:1}, {b:2}],
    reload: ()=>{},
    projectLocked: false

  }

  it('renders without crashing', () => {

     shallow(<Tasks {...props}/>);
   });

  it('should render MUI List', ()=>{

    const wrapper = shallow(<Tasks {...props}/>);
    expect(wrapper.find(List)).toHaveLength(1);
  });


    it('should render Task', ()=>{

      const wrapper = shallow(<Tasks {...props}/>);
      expect(wrapper.find(Task)).toHaveLength(props.tasks.length);
    });


});
