import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import axios from 'axios';
import * as actions from '../../../../../../store/actions/timebyter';

import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import PlayIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';


export class StopWatch extends Component {
  state = {
    counting: false,
    paused: false,
    runningTime :  0,
    currentTaskPartId: null,
    taskpartFinished: false
  }

  setupBeforeUnloadListener = () => {
    window.addEventListener("beforeunload", (ev) => {
        ev.preventDefault();
        return ev.returnValue = 'Please stop watch before exiting!';
    });
    };

  componentDidMount(){
    this.setupBeforeUnloadListener();
  }

  componentWillUnmount(){
    this.handleStop();
    window.removeEventListener("beforeunload", (ev) => {
       this.handleStop();
    });
    clearInterval(this.timer)
  }

  handleClick = () => {
    this.setState(state => {
     if (state.counting) {
       clearInterval(this.timer);
     } else {
       const startTime = Date.now() - state.runningTime;
       this.timer = setInterval(() => {
         this.setState({ runningTime: Date.now() - startTime });
       });
     }
     return { counting: !state.counting }
   });
 };

  handlePlay= () => {
    //UI
      this.handleClick()
      this.props.startTimetracking()
    //create new taskpart in database
    axios.post("http://127.0.0.1:8000/api/taskparts/",
    {task: this.props.taskId},
    {headers: {'Authorization': `Token ${this.props.token}`}}
    )
      .then(response=>{
        console.log(response.data)
        this.setState({currentTaskPartId:response.data.id, taskpartFinished: false})
      })
      .catch(error=>{
        console.log(error)
      })
  }

  handleStop= () => {
      //UI
      this.handleClick()
      this.props.stopTimetracking()
      this.setState({runningTime:0})
      //update new taskpart in database
      if(this.state.currentTaskPartId && !this.state.taskpartFinished){
          axios.get(`http://127.0.0.1:8000/api/taskparts/${this.state.currentTaskPartId}/stopped/`,
          {headers: {'Authorization': `Token ${this.props.token}`}})
            .then(response=>{
              this.setState({taskpartFinished: true, currentTaskPartId: null})
              console.log(response.data)
              this.props.reload()
            })
            .catch(error=>{
              console.log(error)
            })
      }

  }

  convertMs = (ms) => {
    //let days = Math.floor(ms / (24*60*60*1000));
    let daysms=ms % (24*60*60*1000);
    let hours = Math.floor((daysms)/(60*60*1000)).toString();
    let hoursms=ms % (60*60*1000);
    let minutes = Math.floor((hoursms)/(60*1000)).toString();
    let minutesms=ms % (60*1000);
    let sec = Math.floor((minutesms)/(1000)).toString();

    if (hours.length < 2) {
      hours = "0" + hours;
    }

    if (minutes.length < 2) {
      minutes = "0" + minutes;
    }

    if (sec.length < 2) {
      sec = "0" + sec;
    }

    return hours+":"+minutes+":"+sec;
  }

  render(){
    let icon = null;
    if(!this.props.timetrackingOn){
    icon = (
      <IconButton color="primary">
          <PlayIcon onClick={this.handlePlay} />
      </IconButton>
    )
    }

    if (this.state.counting) {
      icon = (
        <React.Fragment>
          <IconButton color="primary">
              <StopIcon onClick={this.handleStop} />
          </IconButton>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        <Typography variant='body1'>
        {this.convertMs(this.state.runningTime)}
        {icon}
        </Typography>

      </React.Fragment>
    );
  }
}


const mapStateToProps = state => {
  return {
    timetrackingOn: state.timebyter.timetrackingOn,
    token: state.auth.token
  }
}

const mapDispatchToProps = dispatch => {
  return {
    startTimetracking: () => dispatch(actions.startTimeTracking()),
    stopTimetracking: () => dispatch(actions.stopTimeTracking())
  };
}

StopWatch.propTypes = {
  taskId: PropTypes.number.isRequired,
  reload: PropTypes.func.isRequired,
};

export default  (connect(mapStateToProps,mapDispatchToProps)(StopWatch));
