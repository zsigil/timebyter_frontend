import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import PlayIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';

import {StopWatch} from './StopWatch';

configure({ adapter: new Adapter() });


describe('<StopWatch />', ()=>{
  const props = {
    taskId: 1,
    reload: ()=>{},
    startTimetracking: ()=>{},
    stopTimetracking: ()=>{},
  }

  it('renders without crashing', () => {

     shallow(<StopWatch {...props}/>);
   });

  it('should render Play icon, if timetracking is off', ()=>{

    const wrapper = shallow(<StopWatch {...props}/>);
    wrapper.setState({counting:false})
    wrapper.setProps({timetrackingOn:false})
    expect(wrapper.find(PlayIcon)).toHaveLength(1);
    expect(wrapper.find(StopIcon)).toHaveLength(0);
  });

  it('should render Stop icon, if counting', ()=>{

    const wrapper = shallow(<StopWatch {...props}/>);
    wrapper.setState({counting:true})
    wrapper.setProps({timetrackingOn:true})
    expect(wrapper.find(StopIcon)).toHaveLength(1);
    expect(wrapper.find(PlayIcon)).toHaveLength(0);
  });

  it('should render no icon, if not counting and timetrakcing is on', ()=>{

    const wrapper = shallow(<StopWatch {...props}/>);
    wrapper.setState({counting:false})
    wrapper.setProps({timetrackingOn:true})
    expect(wrapper.find(StopIcon)).toHaveLength(0);
    expect(wrapper.find(PlayIcon)).toHaveLength(0);
  });


});
