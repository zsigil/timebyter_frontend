import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import {Task} from './Task';
import StopWatch from './StopWatch/StopWatch';

configure({ adapter: new Adapter() });


describe('<Task />', ()=>{
  const props = {
    classes: {
      listitemtext: {},
    },
    task: {},
    taskId: 1,
    reload: ()=>{},
    projectLocked: false,
  }

  it('renders without crashing', () => {

     shallow(<Task {...props}/>);
   });

  it('should render <StopWatch/>, if projectLocked is false', ()=>{

    const wrapper = shallow(<Task {...props}/>);
    expect(wrapper.find(StopWatch)).toHaveLength(1);
  });

  it('should NOT render <StopWatch/>,if projectLocked is true', ()=>{

    const wrapper = shallow(<Task {...props} projectLocked={true}/>);
    expect(wrapper.find(StopWatch)).toHaveLength(0);
  });

});
