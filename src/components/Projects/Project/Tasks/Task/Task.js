import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';

import StopWatch from './StopWatch/StopWatch';

const styles = {
  listitemtext:{
    maxWidth:'280px',
    wordWrap: 'break-word'
  },
};

export class Task extends Component {

  update = () => {
    this.forceUpdate();
  }

  render(){
    const taskdescription = (
      <React.Fragment>
        {this.props.task.description}<br/>
        {this.props.task.duration}
      </React.Fragment>
    )
    return (
      <ListItem key={this.props.task.id}>
        <ListItemText className={this.props.classes.listitemtext}
          primary={this.props.task.title}
          secondary={taskdescription}
        />
        {this.props.projectLocked ? null :
        <ListItemSecondaryAction>
          <StopWatch taskId={this.props.taskId} reload={this.props.reload}/>
        </ListItemSecondaryAction>
        }
      </ListItem>
    );
  }
}

Task.propTypes = {
  task: PropTypes.object.isRequired,
  taskId: PropTypes.number.isRequired,
  reload: PropTypes.func.isRequired,
  projectLocked: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Task);
