import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Task from './Task/Task';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';



const styles = {
  root: {
    // width: '100%',
    // maxWidth: 500,
    // textAlign: 'center',
    // margin: 'auto',
  },
};

export class Tasks extends Component {
  render(){
    let taskList =  this.props.tasks.map(task=>(
          <Task
            key={task.id}
            task={task}
            taskId={task.id}
            reload={this.props.reload}
            projectLocked={this.props.projectLocked}
          />
        ))


    return (
      <div className={this.props.classes.root}>
        <List>
          {taskList}
        </List>
      </div>
    );
  }
}

Tasks.propTypes = {
  tasks: PropTypes.array.isRequired,
  reload: PropTypes.func.isRequired,
  projectLocked: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Tasks);
