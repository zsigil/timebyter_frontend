import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/DeleteForever';
import DetailIcon from '@material-ui/icons/DetailsTwoTone';
import AddIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import UnlockIcon from '@material-ui/icons/LockOpen';
import LockIcon from '@material-ui/icons/Lock';
import Tooltip from '@material-ui/core/Tooltip';

import Tasks from './Tasks/Tasks';
import NewTask from './Tasks/NewTask';
import DeleteProject from '../DeleteProject/DeleteProject';
import DetailedProject from '../DetailedProject/DetailedProject';
import EditProject from '../EditProject/EditProject';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  root: {
    width: '80%',
    maxWidth: 700,
    textAlign: 'center',
    margin: 'auto',
  },
    progress: {
      marginTop: '25vh',
      marginLeft: '50%'
    },
});

export class Project extends Component {
  state = {
    project: {},
    addingTask: false,
    deleting : false,
    showDetails: false,
    editing: false,
    loading: true
  }

  componentDidMount(){
    this.loadProject()
  }

  loadProject= () => {
    const projectId = this.props.match.params.id
    axios.get(`http://127.0.0.1:8000/api/detailedprojects/${projectId}/`,
    {headers: {'Authorization': `Token ${this.props.token}`}})
    .then(response=>{
      this.setState({project: response.data, loading: false})



    })
  }


  addNewTask= (task) => {
    if (!task.title) {
      return;
    }
    this.setState({addingTask: true})
    const newtask = {...task, project:this.state.project.id}
    axios.post("http://127.0.0.1:8000/api/tasks/", newtask,
    {headers: {'Authorization': `Token ${this.props.token}`}} )
    .then(response=>{
      console.log(response)
      this.handleClose()
    });
  }

  handleClose = () => {
    this.setState({addingTask:false});
    this.loadProject()
  }

  handleOpen = () => {
    this.setState({addingTask:true});
  }

  handleDelete = () => {
    this.setState({deleting:true});
  }

  cancelDelete = () => {
    this.setState({deleting:false});
  }

  deleteProject = () => {
    axios.delete(`http://127.0.0.1:8000/api/projects/${this.state.project.id}/`,
    {headers: {'Authorization': `Token ${this.props.token}`}} )
    .then(response=>{
      this.setState({deleting:false});
      this.props.history.push('/')
    });
  }

  handleDetail = event => {
    this.setState(state => ({
      showDetails: !state.showDetails,
    }));
  };

  handleEdit = event => {
    this.loadProject()
    this.setState(state => ({
      editing: !state.editing
    }))
  }

  handleLock = () =>{
    const projectId = this.state.project.id
    axios.get(`http://127.0.0.1:8000/api/projects/${projectId}/finished/`,
    {headers: {'Authorization': `Token ${this.props.token}`}})
    .then(response=>{
      this.loadProject()
    })
  }

  handleUnlock= ()=> {
    const projectId = this.state.project.id
    axios.get(`http://127.0.0.1:8000/api/projects/${projectId}/reopen/`,
    {headers: {'Authorization': `Token ${this.props.token}`}})
    .then(response=>{
      this.loadProject()
    })
  }

  render(){
    let tasks = (
      <Typography
        variant="h4"
      >
        No tasks yet. Create a task to start your timetracking!
      </Typography>
    );

    if (this.state.project.tasks){
      tasks = (
        <Tasks
          tasks={this.state.project.tasks}
          reload={this.loadProject}
          projectLocked={this.state.project.finished}
        />
      )
    }

    return(
      <div>
      {this.state.loading ?
        <CircularProgress className={this.props.classes.progress} />
        :

      <div className={this.props.classes.root}>

          <Typography
            variant="h4"
          >
          {this.state.project.title}
            <Tooltip title="details">
              <IconButton onClick={this.handleDetail}>
                  <DetailIcon  />
              </IconButton>
            </Tooltip>
          </Typography>

        {this.state.project.finished ?
          <div>
            <Tooltip title="unlock project">
              <IconButton color="primary" onClick={this.handleUnlock}>
                  <UnlockIcon fontSize="large" />
              </IconButton>
            </Tooltip>

          </div>
           :
          <div>
            <Tooltip title="add new task">
              <IconButton color="primary" onClick={this.handleOpen}>
                  <AddIcon fontSize="large"/>
              </IconButton>
            </Tooltip>

            <Tooltip title="edit project">
              <IconButton color="primary" onClick={this.handleEdit}>
                  <EditIcon fontSize="large"/>
              </IconButton>
            </Tooltip>

            <Tooltip title="delete project">
              <IconButton color="primary" onClick={this.handleDelete}>
                  <DeleteIcon fontSize="large" />
              </IconButton>
            </Tooltip>

            <Tooltip title="lock project">
              <IconButton color="primary" onClick={this.handleLock}>
                  <LockIcon fontSize="large" />
              </IconButton>
            </Tooltip>


          </div>
        }

        {this.state.deleting ?
          <DeleteProject
            cancelDelete={this.cancelDelete}
            deleteProject={this.deleteProject}
            projectName={this.state.project.title}
          /> : null}

        {this.state.showDetails ?
          <DetailedProject
            project={this.state.project}
            showDetails={this.state.showDetails}
            handleDetail={this.handleDetail}
          /> : null}

          {this.state.editing ?
            <EditProject
              project={this.state.project}
              editing={this.state.editing}
              close={this.handleEdit}
          /> : null}

        {tasks}
        <NewTask
          dialogopen={this.state.addingTask}
          handleClose={this.handleClose}
          add={this.addNewTask}
        />

      </div>
    }
    </div>

    );
  }

}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
  }
}

const mapDispatchToProps = dispatch => {
  return {

  };
}

export default  withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(Project));
