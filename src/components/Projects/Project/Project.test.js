import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';

import Tasks from './Tasks/Tasks';
import NewTask from './Tasks/NewTask';
import DeleteProject from '../DeleteProject/DeleteProject';
import DetailedProject from '../DetailedProject/DetailedProject';
import EditProject from '../EditProject/EditProject';
import {Project} from './Project';

configure({ adapter: new Adapter() });


describe('<Project />', ()=>{
  const props = {
    classes: {
      root: {},
      progress:{},
    },
    match:{params:1}
  }

  it('renders without crashing', () => {

     shallow(<Project {...props}/>);
   });

  it('should render <CircularProgress/>, when loading', ()=>{

    const wrapper = shallow(<Project {...props}/>);
    wrapper.setState({loading:true})
    wrapper.setProps({match:{params:1}})
    expect(wrapper.find(CircularProgress)).toHaveLength(1);
  });


    it('should render 5 Iconbuttons, if not loading and project is not finished', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, project:{finished:false}})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(IconButton)).toHaveLength(5);
    });

    it('should render 2 Iconbuttons, if not loading and project is finished', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, project:{finished:true}})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(IconButton)).toHaveLength(2);
    });

    it('should render DeleteProject, if not loading, and deleting', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, deleting:true, project:{title:""}})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(DeleteProject)).toHaveLength(1);
    });

    it('should render DetailedProject, if not loading, and showDetails is true', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, showDetails:true})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(DetailedProject)).toHaveLength(1);
    });

    it('should render EditProject, if not loading, and editing is true', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, editing:true})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(EditProject)).toHaveLength(1);
    });

    it('should render NewTask, if not loading', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(NewTask)).toHaveLength(1);
    });

    it('should render Tasks, if not loading', ()=>{

      const wrapper = shallow(<Project {...props}/>);
      wrapper.setState({loading:false, project:{tasks:[]}})
      wrapper.setProps({match:{params:1}})
      expect(wrapper.find(Tasks)).toHaveLength(1);
    });


});
