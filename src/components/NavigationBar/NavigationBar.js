import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

import mystyles from './NavigationBar.module.css';
import * as actions from '../../store/actions/auth';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing.unit,
    padding: '10px 20px',
    fontSize: '16px',
    borderRadius: '10px',
  },
  appbar: {
    background: 'transparent',
    boxShadow:'none'
  },
  logo: {
    // color: "#D3D3D3",
    color: "silver",
    fontSize: '20px',
    fontFamily: 'cursive'
  },
  paper: {
    borderRadius: '10px'
  },
  link: {
    textDecoration: 'none',
    '&:hover': {textDecoration: 'none',},
    color: 'silver',
    fontSize: '20px',
  }
});

export class NavigationBar extends Component {
  state = {
    open: false
  }

  handleToggle = () => {
  this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
  if (this.anchorEl.contains(event.target)) {
    return;
  }

  this.setState({ open: false });
  };

  render(){
    let navButtons = (
      <React.Fragment>
        <Link  className={this.props.classes.link} to="/" >
          <Button variant="contained" color="secondary" className={this.props.classes.button}>About TimeByter</Button>
        </Link>
        <Link className={this.props.classes.link}  to="/login" >
          <Button variant="contained" color="secondary" className={this.props.classes.button}>Login</Button>
        </Link>
      </React.Fragment>
    );

    if (this.props.authenticated) {
      navButtons = (
        <React.Fragment>
            <Button
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                aria-owns={this.state.open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={this.handleToggle}
                variant="contained"
                color="secondary"
                className={this.props.classes.button}
            >
            {this.props.username}
            </Button>
            <Popper open={this.state.open} anchorEl={this.anchorEl} transition disablePortal>
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                  >
                    <Paper className={this.props.classes.paper}>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList onClick={this.handleToggle}>
                            <MenuItem >
                                <Link className={this.props.classes.link}  to='/projects'>
                                Projects
                                </Link>
                            </MenuItem>

                            <MenuItem >
                              <Link className={this.props.classes.link} to="/profile">
                              Profile
                              </Link>
                            </MenuItem>

                            <MenuItem onClick={this.props.onLogout}>
                                <Link className={this.props.classes.link}  to='/'>
                                  Logout
                                </Link>
                            </MenuItem>

                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
        </React.Fragment>
      );
    }

    return(
      <div className={this.props.classes.root}>
        <div className={mystyles.Navbar}>
          <Grid container justify="space-between">
            <AppBar className={this.props.classes.appbar} position="static">
                <Toolbar>
                  <Grid container justify="flex-start">
                    <Grid item>
                      <Link  className={this.props.classes.link}  to='/projects'>
                          <Button variant="outlined" color="secondary" className={this.props.classes.logo}>TimeByter</Button>
                      </Link>
                    </Grid>
                  </Grid>

                  <Grid container justify="flex-end">
                    <Grid item>
                        {navButtons}
                    </Grid>
                  </Grid>
                </Toolbar>
            </AppBar>
          </Grid>
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    authenticated: state.auth.token !== null,
    username: state.auth.username
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogout:  () => dispatch(actions.logout()),
  };
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(NavigationBar));
