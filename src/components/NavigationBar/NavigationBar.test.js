import { configure, shallow, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

// import { Link } from 'react-router-dom';
import { Link } from 'react-router-dom';
import {NavigationBar} from './NavigationBar';

configure({ adapter: new Adapter() });


describe('<NavigationBar />', ()=>{
  const props = {
    classes: {
      root: {},
      button: {},
      appbar: {},
      logo: {},
      root: {},
      paper: {},
      link: {},
    }
  }

  it('renders without crashing', () => {

     shallow(<NavigationBar {...props}/>);
   });

  it('should render 3 Links if not authenticated', ()=>{
    const wrapper = shallow(<NavigationBar authenticated={false} {...props}/>);
    expect(wrapper.find(Link)).toHaveLength(3);
  });

  it('should render 1 link if authenticated', ()=>{
    const wrapper = shallow(<NavigationBar authenticated={true} {...props}/>);
    // wrapper.setProps({authenticated: true})
    expect(wrapper.find(Link)).toHaveLength(1);
  })
});
