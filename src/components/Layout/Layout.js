import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';


import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blueGrey from '@material-ui/core/colors/blueGrey';
import grey from '@material-ui/core/colors/grey';

import './Layout.css';
import NavigationBar from '../NavigationBar/NavigationBar';
import FootBar from '../FootBar/FootBar';


const theme = createMuiTheme({
  palette: {
    primary: blueGrey,
    secondary: grey,
    type: 'dark',
  },
  typography: {
  useNextVariants: true,
  h4: {color: 'silver', textAlign: 'center'},
  h6: {color: 'silver'},
  body1: {color: 'silver', fontSize: '1.25rem', textAlign:'left'}
  },
  overrides: {
    MuiListItemText: { // Name of the component ⚛️ / style sheet
      primary: { // Name of the rule
        color: 'silver',
        fontSize: '20px' // Some CSS
      },
      secondary: {
        color: '#82b1ff',
      },
    },
    MuiIconButton: {
      colorPrimary: {
        color: 'silver',
      },
    },
    MuiInputBase: {
      input: {
        color: 'silver',
        fontSize: '20px'
      }
    },
  }
});

export class Layout extends Component {
  render(){
    return(
      <MuiThemeProvider theme={theme}>
        <React.Fragment>
          <CssBaseline />
          <NavigationBar />
          <main>
            {this.props.children}
          </main>
          <FootBar />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

export default withRouter(Layout);
