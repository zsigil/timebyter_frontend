import { configure, shallow, mount,render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';


import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import './Layout.css';
import NavigationBar from '../NavigationBar/NavigationBar';
import FootBar from '../FootBar/FootBar';

import {Layout} from './Layout';
import {App} from '../../App';

configure({ adapter: new Adapter() });


describe('<Layout />', ()=>{

  it('renders without crashing', () => {

     shallow(<Layout/>);
   });

  it('should render 1 <MuiThemeProvider/>', ()=>{

    const wrapper = shallow(<Layout/>);
    expect(wrapper.find(MuiThemeProvider)).toHaveLength(1);
  });

  it('should render 1 <CssBaseline/>', ()=>{

    const wrapper = shallow(<Layout/>);
    expect(wrapper.find(CssBaseline)).toHaveLength(1);
  });

  it('should render 1 <NavigationBar/>', ()=>{

    const wrapper = shallow(<Layout/>);
    expect(wrapper.find(NavigationBar)).toHaveLength(1);
  });

  it('should render 1 <FootBar/>', ()=>{

    const wrapper = shallow(<Layout/>);
    expect(wrapper.find(FootBar)).toHaveLength(1);
  });


});
