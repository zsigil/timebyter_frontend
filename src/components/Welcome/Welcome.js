import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom'

import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

// import mystyles from './Welcome.module.css';
import BackTick from '../BackTick/BackTick';

const styles = theme => ({
  root: {
    width: '80%',
    maxWidth: 600,
    textAlign: 'center',
    margin: '25vh auto',
    color: 'silver'
  },

  button: {
    margin: theme.spacing.unit,
    padding: '10px 20px',
    fontSize: '20px',
    borderRadius: '8px'
  },
});

export class Welcome extends Component {
  render(){
    return (
      <div className={this.props.classes.root}>
        <div>
          <Link component={RouterLink} to="/login" underline='none'>
            <Button variant="contained" color="primary" className={this.props.classes.button}>
               Login
            </Button>
          </Link>
          <Link component={RouterLink} to="/signup" underline='none'>
            <Button color="primary" className={this.props.classes.button}>
               Sign Up
            </Button>
          </Link>
          </div>
        <BackTick />
      </div>
    );
  }
}

export default withStyles(styles)(Welcome);
