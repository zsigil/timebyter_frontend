import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

// import { Link } from 'react-router-dom';
import BackTick from '../BackTick/BackTick';
import {Welcome} from './Welcome';

configure({ adapter: new Adapter() });


describe('<Welcome />', ()=>{
  const props = {
    classes: {
      root: {},
      button: {}
    }
  }

  it('renders without crashing', () => {

     shallow(<Welcome {...props}/>);
   });

  it('should render <BackTick/>', ()=>{

    const wrapper = shallow(<Welcome {...props}/>);
    expect(wrapper.find(BackTick)).toHaveLength(1);
  });

});
