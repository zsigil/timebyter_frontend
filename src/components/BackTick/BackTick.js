import React, { Component } from 'react';
import mystyles from './BackTick.module.css';

class BackTick extends Component {
  state = {
    timeToDisplay : '',
    back: 0
  }

  componentDidMount(){
    this.interval = setInterval(this.tickback, 1000)
    this.startertime =  Date.now() //ms!
  }

  componentWillUnmount(){
    clearInterval(this.interval)
  }

  tickback = () => {
    this.setState({back:this.state.back+1000})
    let newTime = this.timeFormatter(new Date(this.startertime-this.state.back))
    this.setState({timeToDisplay:newTime})
  }

  timeFormatter = mydate => {
    //Date object will have these functions
    let hours = mydate.getHours().toString();
    let minutes = mydate.getMinutes().toString();
    let seconds = mydate.getSeconds().toString();

    while(hours.length<2) {
      hours = '0' + hours;
    }

    while(minutes.length<2) {
      minutes = '0' + minutes;
    }

    while(seconds.length<2) {
      seconds = '0' + seconds;
    }
    return `${hours}:${minutes}:${seconds}`
  }


  render(){
    return(
      <div className={mystyles.BackTick}>
        {this.state.timeToDisplay}
      </div>
    );
  }
}

export default BackTick;
