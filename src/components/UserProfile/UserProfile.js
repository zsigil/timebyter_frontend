import React, {Component} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';

const styles = theme => ({
  card: {
  maxWidth: '60%',
  margin: '50px auto',
  },
  section:{
    margin:  `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
  }
});

class UserProfile extends Component {
  state ={
    userlist: [],
    loading: true
  }

  componentDidMount(){
    axios.get('http://127.0.0.1:8000/api/user/',
    {headers: {'Authorization': `Token ${this.props.token}`}})
    .then(response=>{
      this.setState({userlist: response.data, loading: false})
      console.log(this.state.userlist)
    })
  }

  render(){
    const token_expirationTime = localStorage.getItem('expirationDate').split("(")[0];

    return(
      <div>
        {this.state.loading ?
          <CircularProgress />
          :

          <Card className={this.props.classes.card} raised={true}>
           <CardActionArea>
             <CardContent>
              <div className={this.props.classes.section}>
                 <Typography gutterBottom variant="h5" component="h2">
                   {this.state.userlist[0].username}
                 </Typography>
                 <Typography gutterBottom variant="body1" color="primary">
                   Token expires: {token_expirationTime}
                 </Typography>
                 <Typography variant="h6">
                   {this.state.userlist[0].email}
                 </Typography>
               </div>
             <div className={this.props.classes.section}>

               <Typography variant="body1">
                 Last login: {this.state.userlist[0].last_login}
               </Typography>
               <Typography variant="body1">
                 Date joined: {this.state.userlist[0].date_joined}
               </Typography>
             </div>
               <Divider variant="middle"/>

              <div className={this.props.classes.section}>
               <Typography variant="body1">
                 Number of projects: {this.state.userlist[0].number_of_projects}
               </Typography>
               <Typography variant="body1">
                 Number of finished projects: {this.state.userlist[0].number_of_finished_projects}
               </Typography>
             </div>
             </CardContent>
             </CardActionArea>
           </Card>


        }

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
  }
}

export default withStyles(styles)(connect(mapStateToProps,null)(UserProfile));
